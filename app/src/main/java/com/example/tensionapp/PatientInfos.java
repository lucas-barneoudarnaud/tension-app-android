package com.example.tensionapp;

import android.content.SharedPreferences;
import android.util.Log;

import java.util.Date;


public class PatientInfos {

    private String nom = "defaultName";
    private String prenom = "defaultFirstame";
    private String traitement = "defaultTraitment";
    private Date start = new Date();
    private Date end = new Date();

    public static final String SHARED_KEY = "patient_infos";

    private static PatientInfos patientInfos = new PatientInfos();

    private PatientInfos() {}

    public static PatientInfos getInstance() {
        return patientInfos;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTraitement() {
        return traitement;
    }

    public void setTraitement(String traitement) {
        this.traitement = traitement;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public boolean patientInfosSet(SharedPreferences sp) {
        return sp.contains("prenom") && sp.contains("nom") && sp.contains("traitement");
    }

    public void saveInMemory(SharedPreferences sp) {
        SharedPreferences.Editor e = sp.edit();

        e.putString("prenom", prenom);
        e.putString("nom", nom);
        e.putString("traitement", traitement);
//        e.putBoolean("bras", bras);

        e.apply();
    }

    public void restoreFromMemory(SharedPreferences sp) {
        setPrenom(sp.getString("prenom", ""));
        setNom(sp.getString("nom", ""));
        setTraitement(sp.getString("traitement", ""));
    }
}
