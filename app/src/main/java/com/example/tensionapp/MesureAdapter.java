package com.example.tensionapp;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class MesureAdapter extends ArrayAdapter<Mesure> {

    ArrayList<Mesure> mesures;
    Context c;
    int viewId;

    public MesureAdapter(Context context, int resource, ArrayList<Mesure> mesures) {
        super(context, resource, mesures);

        this.mesures = mesures;
        this.c = context;
        this.viewId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi =
                    (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewId, parent, false);
        }

        final Mesure m = mesures.get(position);
        if (m != null) {
            TextView sys = v.findViewById(R.id.mesure_item_sys);
            TextView dis = v.findViewById(R.id.mesure_item_dis);
            TextView pouls = v.findViewById(R.id.mesure_item_pouls);
            TextView date = v.findViewById(R.id.mesure_item_date);

            if (sys != null) {
                sys.setText(String.valueOf(m.getSystolique()));
            }
            if (dis != null) {
                dis.setText(String.valueOf(m.getDiastolique()));
            }
            if (pouls != null) {
                int p;
                if ((p = m.getPouls()) == 0) {
                    pouls.setText(R.string.nullValue);
                } else {
                    pouls.setText(String.valueOf(p));
                }
            }

            if (date != null) {
//                DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, Locale.getDefault());
//                date.setText(df.format(m.getDate()));
                date.setText(m.getDate().toLocaleString());
            }

        }
        return v;


    }
}
