package com.example.tensionapp;

import android.content.SharedPreferences;

public class MesureInfos {

    private String marque = "defaultMarque";
    private String modele = "defaultModele";
    private Boolean poignet = false;
    private Boolean bras = false;

    public static final String SHARED_KEY = "mesure_infos";

    public static MesureInfos mesureInfos = new MesureInfos();

    private MesureInfos() {}

    public static MesureInfos getInstance() {
        return mesureInfos;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public Boolean getPoignet() {
        return poignet;
    }

    public void setPoignet(Boolean poignet) {
        this.poignet = poignet;
    }

    public Boolean getBras() {
        return bras;
    }

    public void setBras(Boolean bras) {
        this.bras = bras;
    }

    public boolean mesureInfosSet(SharedPreferences sp) {
        return sp.contains("marque") && sp.contains("modele") && sp.contains("poignet") && sp.contains("bras");
    }

    public void saveInMemory(SharedPreferences sp) {
        SharedPreferences.Editor e = sp.edit();

        e.putString("marque", marque);
        e.putString("modele", modele);
        e.putBoolean("poignet", poignet);
        e.putBoolean("bras", bras);

        e.apply();
    }

    public void restoreFromMemory(SharedPreferences sp) {
        setMarque(sp.getString("marque", ""));
        setModele(sp.getString("modele", ""));
        setPoignet(sp.getBoolean("poignet", false));
        setBras(sp.getBoolean("bras", false));
    }
}
