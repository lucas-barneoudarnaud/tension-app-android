package com.example.tensionapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.Map;

public class MoyennesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moyennes);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fillMoyennesTab();
    }

    public void fillMoyennesTab() {
        Map day1Mom1 = MesuresList.getMoyenne(1,1);
        Map day1Mom2 = MesuresList.getMoyenne(1,2);
        Map day1Total = MesuresList.getMoyenne(1,0);

        ((TextView) findViewById(R.id.day1_1_syst)).setText(""+day1Mom1.get("moySyst"));
        ((TextView) findViewById(R.id.day1_1_dias)).setText(""+day1Mom1.get("moyDiast"));
        ((TextView) findViewById(R.id.day1_1_pouls)).setText(""+day1Mom1.get("moyPouls"));
        ((TextView) findViewById(R.id.day1_2_syst)).setText(""+day1Mom2.get("moySyst"));
        ((TextView) findViewById(R.id.day1_2_dias)).setText(""+day1Mom2.get("moyDiast"));
        ((TextView) findViewById(R.id.day1_2_pouls)).setText(""+day1Mom2.get("moyPouls"));
        ((TextView) findViewById(R.id.day1_total_syst)).setText(""+day1Total.get("moySyst"));
        ((TextView) findViewById(R.id.day1_total_dias)).setText(""+day1Total.get("moyDiast"));
        ((TextView) findViewById(R.id.day1_total_pouls)).setText(""+day1Total.get("moyPouls"));

        // JOUR 2
        Map day2Mom1 = MesuresList.getMoyenne(2,1);
        Map day2Mom2 = MesuresList.getMoyenne(2,2);
        Map day2Total = MesuresList.getMoyenne(2,0);

        ((TextView) findViewById(R.id.day2_1_syst)).setText(""+day2Mom1.get("moySyst"));
        ((TextView) findViewById(R.id.day2_1_dias)).setText(""+day2Mom1.get("moyDiast"));
        ((TextView) findViewById(R.id.day2_1_pouls)).setText(""+day2Mom1.get("moyPouls"));
        ((TextView) findViewById(R.id.day2_2_syst)).setText(""+day2Mom2.get("moySyst"));
        ((TextView) findViewById(R.id.day2_2_dias)).setText(""+day2Mom2.get("moyDiast"));
        ((TextView) findViewById(R.id.day2_2_pouls)).setText(""+day2Mom2.get("moyPouls"));
        ((TextView) findViewById(R.id.day2_total_syst)).setText(""+day2Total.get("moySyst"));
        ((TextView) findViewById(R.id.day2_total_dias)).setText(""+day2Total.get("moyDiast"));
        ((TextView) findViewById(R.id.day2_total_pouls)).setText(""+day2Total.get("moyPouls"));

        // JOUR 3
        Map day3Mom1 = MesuresList.getMoyenne(3,1);
        Map day3Mom2 = MesuresList.getMoyenne(3,2);
        Map day3Total = MesuresList.getMoyenne(3,0);

        ((TextView) findViewById(R.id.day3_1_syst)).setText(""+day3Mom1.get("moySyst"));
        ((TextView) findViewById(R.id.day3_1_dias)).setText(""+day3Mom1.get("moyDiast"));
        ((TextView) findViewById(R.id.day3_1_pouls)).setText(""+day3Mom1.get("moyPouls"));
        ((TextView) findViewById(R.id.day3_2_syst)).setText(""+day3Mom2.get("moySyst"));
        ((TextView) findViewById(R.id.day3_2_dias)).setText(""+day3Mom2.get("moyDiast"));
        ((TextView) findViewById(R.id.day3_2_pouls)).setText(""+day3Mom2.get("moyPouls"));
        ((TextView) findViewById(R.id.day3_total_syst)).setText(""+day3Total.get("moySyst"));
        ((TextView) findViewById(R.id.day3_total_dias)).setText(""+day3Total.get("moyDiast"));
        ((TextView) findViewById(R.id.day3_total_pouls)).setText(""+day3Total.get("moyPouls"));
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_saisie:
                Intent saisieActivity = new Intent(MoyennesActivity.this, MainActivity.class);
                saisieActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(saisieActivity);
                break;
            case R.id.action_recap:
                Intent recapActivity = new Intent(MoyennesActivity.this, RecapActivity.class);
                recapActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(recapActivity);
                break;
            case R.id.action_infos_mesure:
                Intent mesureInfosActivity = new Intent(MoyennesActivity.this, MesureInfosActivity.class);
                mesureInfosActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mesureInfosActivity);
                break;
            case R.id.action_infos_patient:
                Intent patientInfosActivity = new Intent(MoyennesActivity.this, PatientInfosActivity.class);
                patientInfosActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(patientInfosActivity);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}
