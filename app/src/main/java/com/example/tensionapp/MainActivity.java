package com.example.tensionapp;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MainActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        restoreSavedMesures();

        updateTitle();
        updateAddMeasureBtn();

        showInfosButtons();

        setupClickListenerDateInput();

        // Restauration des sharedPreferences
        MesureInfos.getInstance().restoreFromMemory(getSharedPreferences(MesureInfos.SHARED_KEY, MODE_PRIVATE));
        PatientInfos.getInstance().restoreFromMemory(getSharedPreferences(PatientInfos.SHARED_KEY, MODE_PRIVATE));

        EditText dateInput = findViewById(R.id.saisie_date_input);
        dateInput.setText(new Date().toLocaleString());

        // Enregistre la mesure saisie
        final Button saveMesureBtn = findViewById(R.id.saisie_activity_add_saisie_btn);
        saveMesureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText sysView = findViewById(R.id.systolique_input);
                EditText disView = findViewById(R.id.diastolique_input);
                EditText poulsView = findViewById(R.id.pouls_input);

                Toast t = Toast.makeText(getApplicationContext(), R.string.mesure_saved, Toast.LENGTH_LONG);
                t.setGravity(Gravity.CENTER, 0, 0);

                try {
                    int pouls = poulsView.getText().toString().equals("") ? 0 : Integer.parseInt(poulsView.getText().toString());
                    Date d = customDate == null ? new Date() : customDate;

                    Mesure m = new Mesure(
                            Integer.parseInt(sysView.getText().toString()),
                            Integer.parseInt(disView.getText().toString()),
                            pouls, d, MesuresList.getNumJour(),
                            MesuresList.getNumMesure(),
                            MesuresList.getMoment()
                    );

                    MesuresList.mesures.add(m);
                    MesuresList.mesureDone();
                    updateTitle();
                    t.show();
                } catch (IllegalArgumentException e) {
                    Log.e("Oui", e.getMessage());
                    t.setText(e.getMessage());
                } catch (Exception e) {
                    Log.e("Oui", e.getMessage());
                    t.setText(e.getMessage());
                } finally {
                    t.show();
                }

                updateAddMeasureBtn();
            }
        });
//        FloatingActionButton fab = findViewById(R.id.edit_infos_patient_fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

    }

    // Active ou désactive le bouton en fonction de si toutes les mesures sont prises ou non
    private void updateAddMeasureBtn() {
        Button addBtn = findViewById(R.id.saisie_activity_add_saisie_btn);

        boolean measuresDone = MesuresList.allMesuresDone();
        if (measuresDone) {
            addBtn.setEnabled(false);
            addBtn.setAlpha(0.5f);
        } else {
            addBtn.setEnabled(true);
            addBtn.setAlpha(1);
        }

    }

    // Met à jour le titre en fonction du jour, du num de la mesure et du moment (matin / soir)
    private void updateTitle() {
        TextView title = findViewById(R.id.saisie_activity_saisie_number);
        String moment = "";
        if (MesuresList.getMoment() == 1) {
             moment = getString(R.string.matin);
        } else if (MesuresList.getMoment() == 2) {
            moment = getString(R.string.soir);
        }

        String s = "";
        if (! MesuresList.allMesuresDone()) {
            s = String.format((getResources().getString(R.string.saisie_activity_saisie_number)), MesuresList.getNumJour(), moment, MesuresList.getNumMesure());
        } else {
            s = getResources().getString(R.string.saisie_activity_saisie_end);
        }
        title.setText(s);
    }


    /*** DATEPICKER ***/

    int day, month, year, hour, minute;
    Date customDate;

    private void setupClickListenerDateInput() {
        final EditText et = findViewById(R.id.saisie_date_input);
        et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(MainActivity.this, MainActivity.this, year, month, day);
                dpd.show();
            }
        });
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        this.year = year;
        this.month = month;
        this.day = dayOfMonth;

        Calendar c = Calendar.getInstance();
        this.hour = c.get(Calendar.HOUR_OF_DAY);
        this.minute = c.get(Calendar.MINUTE);

        TimePickerDialog tpd = new TimePickerDialog(MainActivity.this, MainActivity.this, hour, minute, true);
        tpd.show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        this.hour = hourOfDay;
        this.minute = minute;

        Calendar d = new GregorianCalendar(this.year, this.month-1, this.day, this.hour, this.minute);
        this.customDate = d.getTime();

        final EditText et = findViewById(R.id.saisie_date_input);
        et.setText(customDate.toLocaleString());
    }


    /*** BOUTONS INFOS ***/

    /**
     * Affiche ou non les 2 boutons permettant d'éditer
     * les infos Patient ou Mesure
     */
    private void showInfosButtons() {
        FloatingActionButton patientInfosFab = findViewById(R.id.edit_infos_patient_fab);

        if (PatientInfos.getInstance().patientInfosSet(getSharedPreferences(PatientInfos.SHARED_KEY, MODE_PRIVATE))) {
            ViewGroup vg = (ViewGroup) patientInfosFab.getParent();
            vg.removeView(patientInfosFab);
        } else {
            patientInfosFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent patientInfosActivity = new Intent(MainActivity.this, PatientInfosActivity.class);
                    startActivity(patientInfosActivity);
                }
            });
        }

        final FloatingActionButton mesureInfosFab = findViewById(R.id.edit_infos_mesure_fab);
        if (MesureInfos.getInstance().mesureInfosSet(getSharedPreferences(MesureInfos.SHARED_KEY, MODE_PRIVATE))) {
            ViewGroup vg = (ViewGroup) mesureInfosFab.getParent();
            vg.removeView(mesureInfosFab);
        } else {
            mesureInfosFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent patientInfosActivity = new Intent(MainActivity.this, MesureInfosActivity.class);
                    startActivity(patientInfosActivity);
                }
            });
        }
    }

    /*** MENU ***/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_infos_patient:
                Intent patientInfosActivity = new Intent(MainActivity.this, PatientInfosActivity.class);
                patientInfosActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(patientInfosActivity);
                break;
            case R.id.action_infos_mesure:
                Intent mesureInfosActivity = new Intent(MainActivity.this, MesureInfosActivity.class);
                mesureInfosActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mesureInfosActivity);
                break;
            case R.id.action_recap:
                Intent recapActivity = new Intent(MainActivity.this, RecapActivity.class);
                recapActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(recapActivity);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Sauvegarde des mesures
        try {
            MesuresList.saveInfFile(this.openFileOutput(MesuresList.SAVEFILE_NAME, MODE_PRIVATE));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }



    private void restoreSavedMesures() {
        // Restauration des mesures
        try {
            MesuresList.restoreFromFile(this.openFileInput(MesuresList.SAVEFILE_NAME));
        } catch (FileNotFoundException e) {
        }
    }
}
