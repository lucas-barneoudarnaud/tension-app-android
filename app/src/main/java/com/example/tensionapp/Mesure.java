package com.example.tensionapp;

import java.io.Serializable;
import java.util.Date;

public class Mesure implements Serializable {

    private int systolique = 0;
    private int diastolique = 0;
    private int pouls = 0;
    private Date date = new Date();

    private int numJour;
    private int numMesure;
    private int moment;

    private final int MES_MIN = 40;
    private final int MES_MAX = 200;

    public Mesure(int systolique, int diastolique, int pouls, Date date, int numJour, int numMesure, int moment) throws Exception {
        setSystolique(systolique);
        setDiastolique(diastolique);
        setPouls(pouls);
        setDate(date);
        setNumJour(numJour);
        setNumMesure(numMesure);
        setMoment(moment);
    }

    public int getSystolique() {
        return systolique;
    }

    public void setSystolique(int systolique) throws IllegalArgumentException {
        if (systolique >= this.MES_MIN && systolique <= this.MES_MAX) {
            this.systolique = systolique;
        } else {
//            throw new InvalidValueException("Invalid value");
            throw new IllegalArgumentException("Invalid Value");
        }
    }

    public int getDiastolique() {
        return diastolique;
    }

    public void setDiastolique(int diastolique) throws Exception {
        if (diastolique >= this.MES_MIN && diastolique <= this.MES_MAX) {
            this.diastolique = diastolique;
        } else {
            throw new IllegalArgumentException("Invalid Value");
        }
    }

    public int getPouls() {
        return pouls;
    }

    public void setPouls(int pouls) {
        this.pouls = pouls;
    }

    public int getMES_MIN() {
        return MES_MIN;
    }

    public int getMES_MAX() {
        return MES_MAX;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getNumJour() {
        return numJour;
    }

    public void setNumJour(int numJour) {
        this.numJour = numJour;
    }

    public int getNumMesure() {
        return numMesure;
    }

    public void setNumMesure(int numMesure) {
        this.numMesure = numMesure;
    }

    public int getMoment() {
        return moment;
    }

    public void setMoment(int moment) {
        this.moment = moment;
    }
}
