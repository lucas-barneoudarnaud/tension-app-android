package com.example.tensionapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PatientInfosActivity extends AppCompatActivity {

    EditText prenomInput;
    EditText nomInput;
    EditText traitementInput;

    PatientInfos pi = PatientInfos.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_infos);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        prenomInput = findViewById(R.id.patient_prenom_input);
        nomInput = findViewById(R.id.patient_nom_input);
        traitementInput = findViewById(R.id.patient_traitement_input);

        prenomInput.setText(pi.getPrenom());
        nomInput.setText(pi.getNom());
        traitementInput.setText(pi.getTraitement());

        Button saveInfosBtn = findViewById(R.id.save_patient_infos_btn);
        saveInfosBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pi.setPrenom(prenomInput.getText().toString());
                pi.setNom(nomInput.getText().toString());
                pi.setTraitement(traitementInput.getText().toString());

                Toast.makeText(getApplicationContext(), "Sauvegardé", Toast.LENGTH_LONG).show();

                pi.saveInMemory(getSharedPreferences(PatientInfos.SHARED_KEY, MODE_PRIVATE));
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_saisie:
                Intent saisieActivity = new Intent(PatientInfosActivity.this, MainActivity.class);
                saisieActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(saisieActivity);
                break;
            case R.id.action_infos_mesure:
                Intent mesureInfosActivity = new Intent(PatientInfosActivity.this, MesureInfosActivity.class);
                mesureInfosActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mesureInfosActivity);
                break;
            case R.id.action_recap:
                Intent recapActivity = new Intent(PatientInfosActivity.this, RecapActivity.class);
                recapActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(recapActivity);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}
