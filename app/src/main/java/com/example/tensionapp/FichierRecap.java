package com.example.tensionapp;

import android.content.res.Resources;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Map;

public class FichierRecap {

    private static PatientInfos pi = PatientInfos.getInstance();
    private static MesureInfos mi = MesureInfos.getInstance();
    private static ArrayList<Mesure> ml = MesuresList.mesures;

    private static Resources res;

    public static boolean generateFile(FileOutputStream fos, Resources r) {

        res = r;
        if (PatientInfos.getInstance() == null || MesureInfos.getInstance() == null) {
            return false;
        }
        if (ml.size() <= 0) { return false ; }

        PrintStream ps = new PrintStream(fos);

        ps.print(getPatientInfosString());
        ps.println();
        ps.print(getMesureInfosString());
        ps.println();
        ps.print(getRecapHeader());
        ps.println();

        int jour = 1, mesure = 1, moment = 1;
        // Generation du tableau
        for (int i = 1; i <= 9; i++) {

            if (i == 4 || i == 5 || i == 6) jour = 2;
            else if (i == 7 || i == 8 || i == 9) jour = 3;

            if (i == 1 || i == 4 || i == 7) mesure = 1;
            else if (i == 2 || i == 5 || i == 8) mesure = 2;
            else mesure = 3;

            if (mesure == 1) { //1re mesure du jour x
                ps.print(getDayHeader(jour));
            }

            ps.print(getMesureXOfDayY(jour, mesure));

            if (mesure == 3) { // Si la mesure == 3, fin du tableau jour x, donc affichage des moyennes
                ps.print(getMoyennes(jour));
                ps.println();
            }


        }

        ps.close();

        try {
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private static String getRecapHeader() {

        return "--- Relevés --- \n";
    }


    private static String getMesureXOfDayY(int jour, int mesure) {
        Mesure m1 = ml.get((jour-1)*6 + mesure -1); // Mesure x du jour y, matin
        Mesure m2 = ml.get((jour-1)*6 + mesure +3 -1); // Mesure x du jour y, soir

        String s = "Mesure "+mesure+"   |  "+m1.getSystolique()+"  |  "+m1.getDiastolique()+"  |   "+m1.getPouls()+"   ||  "+m2.getSystolique()+"  |  "+m2.getDiastolique()+"  |   "+m2.getPouls()+"   |"+"\n";

        return s;
    }


    public static String getFileName() {
        PatientInfos pii = PatientInfos.getInstance();
        return pii.getPrenom() + "_" + pii.getPrenom()+".txt";
    }

    private static String getPatientInfosString() {
        String s =
                "--- "+res.getString(R.string.title_activity_patient_infos)+" ---\n"+
                pi.getPrenom() + " " + pi.getNom() + "\n"+
                "Relevé du "+ ml.get(0).getDate().toLocaleString() + " au " + ml.get(ml.size()-1).getDate().toLocaleString() + "\n"+
                "Traitement : " + pi.getTraitement() + "\n";

        return s;
    }

    private static String getMesureInfosString() {
        String s =
                "--- "+res.getString(R.string.title_activity_mesure_infos)+" ---"+ "\n"+
                res.getString(R.string.marque) + " : "+ mi.getMarque() + "\n"+
                res.getString(R.string.modele) + " : "+ mi.getModele() + "\n"+
                res.getString(R.string.poignet) + " : "+ (mi.getPoignet() ? "✓" : "X") + "\n"+
                res.getString(R.string.bras) + " : " + (mi.getBras() ? "✓" : "X")+ "\n";

        return s;
    }

    private static String getDayHeader(int numDay) {
        String s = "-----------+-----------------------++-----------------------|\n";
        s+=     res.getString(R.string.jour)+" "+numDay+"     |         "+res.getString(R.string.matin)+"         ||         "+res.getString(R.string.soir)+"          |"+ "\n"+
                "           | "+res.getString(R.string.sys_abbr)+" | "+res.getString(R.string.dias_abbr)+" | "+res.getString(R.string.pouls)+" || "+res.getString(R.string.sys_abbr)+" | "+res.getString(R.string.dias_abbr)+" | "+res.getString(R.string.pouls)+" |"+ "\n";
        return s;
    }

    private static String getMoyennes(int jour) {
        String s = "===========+=======================++=======================|\n";

        Map moyMes1 = MesuresList.getMoyenne(jour, 1);
        Map moyMes2 = MesuresList.getMoyenne(jour, 2);
        Map moyTotale = MesuresList.getMoyenne(jour, 0);

        s += "MoyennesActivity   |  "+moyMes1.get("moySyst")+"  |  "+moyMes1.get("moyDiast")+"  |  "+moyMes1.get("moyPouls")+"  ||  "+moyMes2.get("moySyst")+"  |  "+moyMes2.get("moyDiast")+"  |  "+moyMes2.get("moyPouls")+"  |\n";
        s += "Total      |  "+moyTotale.get("moySyst")+"  |  "+moyTotale.get("moyDiast")+"  |  "+moyTotale.get("moyPouls")+"  ||\n";
        s += "===========+=======================++=======================|\n";

        return s;
    }
}
