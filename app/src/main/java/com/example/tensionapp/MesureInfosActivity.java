package com.example.tensionapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class MesureInfosActivity extends AppCompatActivity {

    EditText marqueInput;
    EditText modeleInput;
    CheckBox poignetInput;
    CheckBox brasInput;
    MesureInfos mi = MesureInfos.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mesure_infos);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        marqueInput = findViewById(R.id.mesure_marque_input);
        modeleInput = findViewById(R.id.mesure_modele_input);
        poignetInput = findViewById(R.id.poignet_input);
        brasInput = findViewById(R.id.bras_input);
//        mi = MesureInfos.getInstance();

        marqueInput.setText(mi.getMarque());
        modeleInput.setText(mi.getModele());
        poignetInput.setChecked(mi.getPoignet());
        brasInput.setChecked(mi.getBras());

        Button saveInfosBtn = findViewById(R.id.save_mesure_infos_btn);
        saveInfosBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mi.setMarque(marqueInput.getText().toString());
                mi.setModele(modeleInput.getText().toString());
                mi.setPoignet(poignetInput.isChecked());
                mi.setBras(brasInput.isChecked());

                Toast.makeText(getApplicationContext(), "Sauvegardé", Toast.LENGTH_LONG).show();

                mi.saveInMemory(getSharedPreferences(MesureInfos.SHARED_KEY, MODE_PRIVATE));
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_infos_patient:
                Intent patientInfosActivity = new Intent(MesureInfosActivity.this, PatientInfosActivity.class);
                patientInfosActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(patientInfosActivity);
                break;
            case R.id.action_saisie:
                Intent saisieActivity = new Intent(MesureInfosActivity.this, MainActivity.class);
                saisieActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(saisieActivity);
                break;
            case R.id.action_recap:
                Intent recapActivity = new Intent(MesureInfosActivity.this, RecapActivity.class);
                recapActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(recapActivity);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}
