package com.example.tensionapp;

import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class MesuresList {

    public static ArrayList<Mesure> mesures = new ArrayList<>();

    public static String SAVEFILE_NAME = "SAVE_MESURE";

    private static int numMesure = 1;
    private static int numJour = 1;
    private static int moment = 1; // 1 = MATIN, 2 = SOIR


    public static void mesureDone() {

        if (numMesure == 3) {
            if (moment == 1) {
                moment = 2;
            } else if (moment == 2) {
                numJour++; // Jour suivant
                moment = 1; // Reset au matin

            }
        }
        numMesure = numMesure == 3 ? 1 : numMesure+1;
    }

    // True si les 3*2*3 (18) mesures sont faites
    public static boolean allMesuresDone() {
        return (numMesure == 3 && numJour == 3 && moment == 2) || numJour > 3;
    }

    public static int getNumMesure() {
        return numMesure;
    }

    public static int getNumJour() {
        return numJour;
    }

    public static int getMoment() {
        return moment;
    }

    /**
     * Retourne un tableau des moyennes de syst, diast et pouls en fonction du jour et du moment
     * Moment = 0 pour matin ET soir
     * Jour = 0 pour les 3 jours (si jour = 0, moment = 0)
     * @param jour int
     * @param moment int
     * @return Map
     */
    public static Map getMoyenne(int jour, int moment) {

        if (jour == 0) { moment = 0; }

        int sommeSyst = 0;
        int sommeDiast = 0;
        int nbValidMesures = 0; // Pour calculer moyenne Syst et Diast
        int sommePouls = 0;
        int nbPoulsValides = 0; // Pour calculer moyenne Pouls

        for (Mesure m : mesures) {
            if (jour == 0) {
                sommeSyst += m.getSystolique();
                sommeDiast += m.getDiastolique();
                nbValidMesures++;
                if (m.getPouls() != 0 ) {
                    sommePouls += m.getPouls();
                    nbPoulsValides++;
                }
            } else {
                if (m.getNumJour() == jour) {

                    if (moment == 0) {
                        sommeSyst += m.getSystolique();
                        sommeDiast += m.getDiastolique();
                        nbValidMesures++;
                        if (m.getPouls() != 0 ) {
                            sommePouls += m.getPouls();
                            nbPoulsValides++;
                        }
                    } else {
                        if (m.getMoment() == moment) {
                            sommeSyst += m.getSystolique();
                            sommeDiast += m.getDiastolique();
                            nbValidMesures++;
                            if (m.getPouls() != 0 ) {
                                sommePouls += m.getPouls();
                                nbPoulsValides++;
                            }
                        }
                    }
                }
            }
        }

        Map<String, Integer> map = new HashMap<String, Integer>();

        map.put("moySyst", nbValidMesures != 0 ? sommeSyst/nbValidMesures : 0);
        map.put("moyDiast", nbValidMesures != 0 ? sommeDiast/nbValidMesures : 0);
        map.put("moyPouls", nbPoulsValides != 0 ? sommePouls/nbPoulsValides : 0);

        return map;
    }

    public static void saveInfFile(FileOutputStream fos) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(mesures);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void restoreFromFile(FileInputStream fis) {
        try {
            ObjectInputStream ois = new ObjectInputStream(fis);

            mesures = (ArrayList<Mesure>) ois.readObject();

            updateGlobalMoment();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void updateGlobalMoment() {
        int nbMesures = mesures.size();

        if (nbMesures >= 6 && nbMesures < 12) numJour = 2;
        else if (nbMesures >= 12 && nbMesures < 18) numJour = 3;
        else if (nbMesures >= 18) numJour = 4;

        if (nbMesures % 3 == 0) numMesure = 1;
        else if (nbMesures % 3 == 1) numMesure = 2;
        else if (nbMesures % 3 == 2) numMesure = 3;

        if (nbMesures < 3) moment = 1;
        else if (nbMesures < 6) moment = 2;
        else if (nbMesures < 9) moment = 1;
        else if (nbMesures < 12) moment = 2;
        else if (nbMesures < 15) moment = 1;
        else moment = 2;
    }
}

class CustomComparator implements Comparator<Mesure> {

    @Override
    public int compare(Mesure o1, Mesure o2) {
        return o1.getDate().compareTo(o2.getDate());
    }
}
