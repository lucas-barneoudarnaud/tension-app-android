package com.example.tensionapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.util.Map;

public class RecapActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recap);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        updateMesuresList();

        TextView mSys = findViewById(R.id.mesures_list_sys_moy);
        TextView mDias = findViewById(R.id.mesures_list_dias_moy);
        TextView mPouls = findViewById(R.id.mesures_list_pouls_moy);

        Map moy = MesuresList.getMoyenne(0,0);

        mSys.setText(String.valueOf(moy.get("moySyst")));
        mDias.setText(String.valueOf(moy.get("moyDiast")));
        mPouls.setText(String.valueOf(moy.get("moyPouls")));

        FloatingActionButton genRecapFileBtn = findViewById(R.id.generate_recap_file_fab);
        genRecapFileBtn.setOnClickListener(this);
        findViewById(R.id.show_moyennes_btn).setOnClickListener(this);
    }

    private void updateMesuresList() {
        MesureAdapter ma = new MesureAdapter(this, R.layout.mesure_list_item, MesuresList.mesures);

        final ListView lv = findViewById(R.id.mesure_list);

        lv.setAdapter(ma);
    }
    
    


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_saisie:
                Intent saisieActivity = new Intent(RecapActivity.this, MainActivity.class);
                saisieActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(saisieActivity);
                break;
            case R.id.action_infos_mesure:
                Intent mesureInfosActivity = new Intent(RecapActivity.this, MesureInfosActivity.class);
                mesureInfosActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mesureInfosActivity);
                break;
            case R.id.action_infos_patient:
                Intent patientInfosActivity = new Intent(RecapActivity.this, PatientInfosActivity.class);
                patientInfosActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(patientInfosActivity);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.generate_recap_file_fab:
                Toast t = Toast.makeText(this, "Fichier généré", Toast.LENGTH_LONG);
                t.setGravity(Gravity.CENTER,0,0);

                if (! MesuresList.allMesuresDone()) {
                    t.setText("Impossible de générer le fichier : Toutes les mesures n'ont pas été saisies");
                } else {
                    if (generateRecapFile()) {
                        t.setText("Fichier généré avec succès");
                    } else {
                        t.setText("Une erreur est survenue");
                    }
                }
                t.show();
                break;

            case R.id.show_moyennes_btn:

                Intent moyennesActivity = new Intent(RecapActivity.this, MoyennesActivity.class);
                moyennesActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(moyennesActivity);
                break;
        }
    }

    private boolean generateRecapFile() {
        try {
            return FichierRecap.generateFile(openFileOutput(FichierRecap.getFileName(), Context.MODE_PRIVATE), getResources());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }
}
